package org.test.service;

import org.apache.dubbo.config.annotation.Reference;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;

@Service
public class DemoService {
    @Reference(version = "1.0.0", timeout = 60000)
    private ITestService testService;
    private final static Logger logger = LoggerFactory.getLogger(DemoService.class);

    public Object seataCommit() {
        testService.Commit();
        return true;
    }

    public Object commit() {
        return testService.Commit();
    }
}
