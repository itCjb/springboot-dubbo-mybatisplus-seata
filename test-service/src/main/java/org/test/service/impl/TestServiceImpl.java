package org.test.service.impl;

import java.util.Date;

import org.apache.dubbo.config.annotation.Service;
import org.springframework.transaction.annotation.Transactional;
import org.test.entity.Test;
import org.test.mapper.TestMapper;
import org.test.service.ITestService;

import com.baomidou.mybatisplus.core.toolkit.Wrappers;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;

@Service(version = "1.0.0", interfaceClass = ITestService.class, timeout = 60000)
public class TestServiceImpl extends ServiceImpl<TestMapper, Test> implements ITestService {

    @Override
    @Transactional
    public Object Commit() {
        Test t2 = getOne(Wrappers.<Test>query().eq("id", 1).last("for update"));
        t2.setOne(String.valueOf(new Date().getTime()));
        t2.setTwo(String.valueOf(Integer.valueOf(t2.getTwo()) + 1));
        updateById(t2);
        return t2.getTwo();
    }

}
